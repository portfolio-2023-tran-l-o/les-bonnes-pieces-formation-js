// Récupération des pièces depuis le fichier JSON
const reponse = await fetch("pieces-autos.json");
const pieces = await reponse.json();

// Procédure qui affiche les pièces 
function genererPieces(pieces) {
    for(let i = 0; i < pieces.length; i++){

        const article = pieces[i];
    
    
        const imageElement = document.createElement("img");
        // on crée un element img
        imageElement.src = article.image;
        // la source de l'image est
    
        const nomElement = document.createElement("h2");
        nomElement.innerText = article.nom;
    
        const prixElement = document.createElement("p");
        prixElement.innerText = `Prix: ${article.prix} € (${article.prix < 35 ? "€" : "€€€"})`;
        // équivalent à "Prix: " + article.prix+ " €";
    
        const categorieElement = document.createElement("p");
        categorieElement.innerText = article.categorie ?? "(aucune catégorie)";
    
    
        const descriptionElement = document.createElement("p");
        descriptionElement.innerText = article.description ?? "(Pas de description pour le moment)";
    
        const disponibiliteElement = document.createElement("p");
        disponibiliteElement.innerText = article.disponibilite ? "En stock" : "Rupture de stock";
    
    
        const sectionFiches = document.querySelector(".fiches");
        // le fait d'avoir mit un point : (".fiches") => class="fiches"
        // sans avoir mit le point  : ("fiches") => <fiches> </fiches> mais ne fonctionne pas car l'élément n'existe pas, il faut donc d'abords createElement
    
        const pieceElement = document.createElement("article");
        // création nouvel élément HTML vide => <article> </article>
        pieceElement.appendChild(imageElement);
        pieceElement.appendChild(nomElement);
        pieceElement.appendChild(prixElement);
        pieceElement.appendChild(categorieElement);
        pieceElement.appendChild(descriptionElement);
        pieceElement.appendChild(disponibiliteElement);
    
        sectionFiches.appendChild(pieceElement);
        // dans la class="fiches" on ajoute la balise article
    }
}

const boutonTrier = document.querySelector(".btn-trier");
// Dès le clique du bouton, on lance l'événement qui trie les pièces par prix croissant
boutonTrier.addEventListener("click", function () {
    const piecesOrdonnee = Array.from(pieces);
    piecesOrdonnee.sort(function (a,b) { 
        return a.prix - b.prix;
    });
    //console.log(pieces);
    //console.log(piecesOrdonnee);

    // Efface le contenu de la balise body et donc l’écran
    document.querySelector(".fiches").innerHTML = '';
    //Ensuite on affiche les produits des pieces de manière ordonnee
    genererPieces(piecesOrdonnee);
})

const boutonTrierDecroissant = document.querySelector(".btn-trier-decroissant");
// Dès le clique du bouton, on lance l'événement qui trie les pièces par prix décroissant
boutonTrierDecroissant.addEventListener("click", function () {
    const piecesOrdonnee = Array.from(pieces);
    piecesOrdonnee.sort((a, b) => b.prix - a.prix); // fonction lambda "(param1,param2) => (return)"
    //console.log(pieces);
    //console.log(piecesOrdonnee);
    document.querySelector(".fiches").innerHTML = '';
    genererPieces(piecesOrdonnee);
})

const boutonFiltrer = document.querySelector(".btn-filtrer");
// Dès le clique du bouton, on lance l'événement qui n'affiche que les prix <= à 35
boutonFiltrer.addEventListener("click", function () {
    const piecesFiltres = pieces.filter(function (piece) {
        return piece.prix <= 35;
    });
    //console.log(pieces);
    //console.log(piecesFiltres);
    document.querySelector(".fiches").innerHTML = '';
    genererPieces(piecesFiltres);

})

const boutonFiltrerDescription = document.querySelector(".btn-filtre-description");
// Dès le clique du bouton, on lance l'événement qui n'affiche que les pièces qui possèdent une description
boutonFiltrerDescription.addEventListener("click", () => {
    const boutonFiltrerDescription = pieces.filter(piece => piece.description); // fonction lambda "param => (return)"
    //console.log(pieces);
    //console.log(boutonFiltrerDescription);
    document.querySelector(".fiches").innerHTML = '';
    genererPieces(boutonFiltrerDescription);
    
})

//const nomsdespieces = pieces.map(piece => piece.prix <= 35 ? piece.nom : null); // récupère que les noms des pieces mais ne rearrange pas la taille du tableau
// donc on devra utiliser la méthode splice
const nomsdespieces = pieces.map(piece => piece.nom);
for(let i = pieces.length -1 ; i >= 0; i--){
    if(pieces[i].prix > 35){
        nomsdespieces.splice(i,1)
    }
 }

/*
// Création de la liste
const abordalesElements = document.createElement("ul");
// Ajout de chaque nom à la liste
for (let i = 0; i < nomsdespieces.length; i++){
    if (nomsdespieces[i] <= 35) {
        nomsdespieces.splice(i, 1); // c'est mieux de utiliser filtre comme à la ligne 70 car plus simple et rapide
    }
    console.log(nomsdespieces);
}
Voir le code juste au dessous sinon*/


const abordablesElements = document.createElement('ul');
for (let i = 0; nomsdespieces.length > i; i++){
    const nomElement = document.createElement('li');
    nomElement.innerText = nomsdespieces[i];
    abordablesElements.appendChild(nomElement);
}
// Ajout de l'en-tête puis de la liste au bloc résultats filtres
document.querySelector('.abordables').appendChild(abordablesElements)


const tableauPrixDisponible = pieces.map(piece => piece.prix);
const tableauNomDisponible = pieces.map(piece => piece.nom);

for (let i = 0; pieces.length > i; i++){
    if (!pieces[i].disponibilite) {
        tableauNomDisponible.splice(i, 1);
        tableauPrixDisponible.splice(i, 1);
    }
}

const disponibleElement = document.createElement('ul');
for (let i = 0; tableauNomDisponible.length > i; i++){
    const pieceElement = document.createElement('li');
    pieceElement.innerText = `${tableauNomDisponible[i]} - ${tableauPrixDisponible[i]} €`;
    disponibleElement.appendChild(pieceElement);
}

document.querySelector('.disponibles').appendChild(disponibleElement);

const InputPrixMax = document.querySelector('#prix-max'); // on prends le id = prix-max
InputPrixMax.addEventListener("input", function () {
    const piecesFiltres = pieces.filter(piece => piece.prix <= InputPrixMax.value) // fonction lambda avec condition
    document.querySelector(".fiches").innerHTML = '';
    genererPieces(piecesFiltres);
});
