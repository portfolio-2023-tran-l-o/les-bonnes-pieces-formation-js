[COURS OPENCLASSROOM Créez des pages web dynamiques avec JavaScript](https://openclassrooms.com/fr/courses/7697016-creez-des-pages-web-dynamiques-avec-javascript)  

+ `Exploitation des données au format JSON`
![json.JPG](images/json.JPG)
+ `NULLISH` : L'opérateur de coalescence des nuls ( ?? ) 
+ `Fonction lambda` : pas besoin d'avoir les mots clés function et return
+ `document`
+ + ```js
    document.querySelector(".exemple"); // html class="exemple"
    document.querySelector("#exemple"); // html id = exemple
    document.querySelector("xyz"); // html <xyz> </xyz>
    // Si la balise xyz n'existe pas il faut d'abords la crée avec createElement
    ```
+ + ```js
    const unePiece = document.createElement("article");
    const photo = document.createElement("img");
    photo.src = "chemin vers l'image";
    const titre = document.createElement("h1")
    titre.innerText = "Les Bonnes Pièces";
    ```
![document.JPG](images/document.JPG)
![index.JPG](images/index.JPG)
## Installation

Après avoir cloné le repo vous avez plusieurs options pour lancer le projet. 

Si vous utiliser VSCode ou un autre éditeur de code avec une extension de serveur web comme live server, vous pouvez lancer direcement votre site avec l'extension que vous utilisez habituellement. 

Dans le cas contraire vous pouvez installer les dépendances de ce projet avec `npm install` puis lancer le projet via la commande `npm start`. Vous verrez dans le termninal le lien vers le site (par defaut http://127.0.0.1:8080 )
## Menu du site

![one.JPG](images/one.JPG)
## Après avoir cliquer sur le bouton "`Trier par prix croissant`"
![two.JPG](images/two.JPG)
## Après avoir cliquer sur le bouton "`Trier par prix décroissant`"
![three.JPG](images/three.JPG)
## Après avoir cliquer sur le bouton "`Filtrer les pièces non abordables` (inférieur ou égal à 35)"
![five.JPG](images/five.JPG)
## Après avoir cliquer sur le bouton "`Filtrer les pièces ayant une description`"
![six.JPG](images/six.JPG)
## Filtrer par rapport au prix avec une barre progressive Max: 60€
### Filtre positionné à 60€
![seven.JPG](images/seven.JPG)
### Filtre positionné à 30€
![eight.JPG](images/eight.JPG)
